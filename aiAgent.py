"""
Created by MorfiWifi AKA morteza eydipour (9811634)
Ai logic will be here !
Using no background thread currently
"""
from judge import Judge
from utill import Constants, alpha_num, alpha_num_rev, save_on_file_log
from game_pices import Board, Cell
import time, random, copy


# import copy


# logical agent
class Ai:
    __neta = 0.001  # learning Value !!
    __name = "Agent1"
    __isHuman = True  # human -> user choose | cpu -> auto eval
    __color = Constants.RED  # color of pices!
    __sleepy_ai = False  # will sleep before moove!
    __is_random_ai = False  # Wont Work Random (normally)
    w0 = -4  # base
    w1 = 5  # normal me
    w2 = -10  # normal Enemy
    w3 = 20  # king me
    w4 = -20  # king enemy
    w5 = -2  # will Die me
    w6 = 2  # will Die enemy
    __my_last_move_history = {"NON": 0}
    is_Train_mod = True

    # Update knowledge from saved file {not w0 - w6}!!
    def set_data_in_knowldege(self, key, value):
        self.__my_last_move_history[key] = value

    def get_agent_knowledge(self) -> dict:
        return self.__my_last_move_history

    def __init__(self, name: str, isHuman: bool, color: str):
        self.__name = name
        self.__color = color
        self.__isHuman = isHuman

    # subtract ?
    def __is_sub(self, var) -> bool:
        if var < 0.5:
            return True
        return False

    # sum or sub tract value !
    def __sum_value(self, ISub: bool, val1, val2):
        if ISub:
            return val1 - val2
        else:
            return val1 + val2

    # w0 - w6 woul be +- 05
    def random_initialize_weights(self):
        randomness = 0.5
        self.w0 = self.__sum_value(self.__is_sub(random.random()), self.w0, random.random() * randomness)
        self.w1 = self.__sum_value(self.__is_sub(random.random()), self.w1, random.random() * randomness)
        self.w2 = self.__sum_value(self.__is_sub(random.random()), self.w2, random.random() * randomness)
        self.w3 = self.__sum_value(self.__is_sub(random.random()), self.w3, random.random() * randomness)
        self.w4 = self.__sum_value(self.__is_sub(random.random()), self.w4, random.random() * randomness)
        self.w5 = self.__sum_value(self.__is_sub(random.random()), self.w5, random.random() * randomness)
        self.w6 = self.__sum_value(self.__is_sub(random.random()), self.w6, random.random() * randomness)

    #  what is your color ? (i am red!)
    def getColor(self) -> str:
        return self.__color

    def getName(self) -> str:
        return self.__name

    def __determine_next_move(self, board: Board, judge: Judge) -> str:
        list_moves = self.__list_nex_moves(board)
        # print("list Of Ai moves:", list_moves)
        dict = {}
        for i in range(len(list_moves)):
            (sy, sx, ey, ex) = list_moves[i]
            clone_cells = board.clonei()
            board_clone = Board()
            board_clone.cells = clone_cells
            move = alpha_num_rev.get(sx) + str(sy + 1) + alpha_num_rev.get(ex) + str(ey + 1)
            # print("Ai cpu move :", move)
            Judge().isValidMove(board_clone, move, self.__color, False)
            value = self.board_value(board_clone)
            # print("Ai Move Value:", value)
            dict[move] = value  # value of move can say ...

        if len(dict.keys()) == 0:
            print("NO MOVE : ")
            return "NN"
        # print(dict)
        best_move = self.get_best_move_from_dict(dict)
        print("best move is:", best_move)
        return best_move

    # next move based on Board current condition & our side
    def next_moove(self, board: Board, judge: Judge):

        # human should write the move ..........
        if self.__isHuman:
            move = input("Write Your MOVE " + self.__name + " :")
            while (not judge.isValidMove(board, move, self.__color, False)):
                move = input("Write Your MOVE " + self.__name + " :")
            else:
                judge.add_history(move)
                # print("Move accepted")
        # CPU shoold find proper move as next ..........
        else:
            move = self.__determine_next_move(board, judge)
            if move == "NN":
                judge.cant_find_move(self.__color)
                print("no MOVE FOUND")
                return
            while (not judge.isValidMove(board, move, self.__color, False)):
                move = self.__determine_next_move(board, judge)
            else:
                judge.add_history(move)
                print("Move accepted")
            # time.sleep(0.1)
            pass

    def __list_nex_moves(self, board: Board) -> [(int, int, int, int)]:
        list_total = []
        ju = Judge()
        for i in range(8):
            for j in range(8):
                cell = board.cells[i][j]
                if cell.get_player_color() != self.__color or (not cell.isActive):
                    continue
                # cell is For Player ---------------
                list_moves = ju.list_near_moves_start_dest(board, i, j, self.__color, cell.isKing())
                # print("moves Y:", i, "X:", j, " -> ", list_moves)
                if list_moves != None and len(list_moves) > 0:
                    list_total = list_total + list_moves
                    # list_total.append(list_moves)
        return list_total

    # save Board point in This Ai instance
    def save_board_values(self, board: Board):
        normal_me = 0
        king_me = 0
        normal_enemy = 0
        king_enemy = 0
        will_die_me = self.num_will_die_my_pices(board)
        will_die_enemy = self.num_will_die_my_enemy(board)

        for i in range(8):
            for j in range(8):
                cell = board.cells[i][j]
                if not cell.isActive:
                    continue
                if cell.get_player_color() == self.__color:
                    if cell.isKing():
                        king_me += 1
                    else:
                        normal_me += 1
                elif cell.get_player_color() != Constants.UNKNOWN:
                    if cell.isKing():
                        king_enemy += 1
                    else:
                        normal_enemy += 1

        self.__my_last_move_history[Constants.normal_me] = normal_me
        self.__my_last_move_history[Constants.king_me] = king_me
        self.__my_last_move_history[Constants.normal_enemy] = normal_enemy
        self.__my_last_move_history[Constants.king_enemy] = king_enemy
        self.__my_last_move_history[Constants.will_die_me] = will_die_me
        self.__my_last_move_history[Constants.will_die_enemy] = will_die_enemy
        self.__my_last_move_history[Constants.base] = self.w0

        knowledge = {"w0": self.w0,
                     "w1": self.w1,
                     "w2": self.w2,
                     "w3": self.w3,
                     "w4": self.w4,
                     "w5": self.w5,
                     "w6": self.w6}

        for key in knowledge.keys():  # save w0 - w6 in temp
            self.__my_last_move_history[key] = knowledge[key]

        if self.w0 != self.w0:
            print("W0 is nan")

        board_value = self.w0 + self.w1 * normal_me + self.w2 * normal_enemy + self.w3 * king_me + self.w4 * king_enemy + self.w5 * will_die_me + self.w6 * will_die_enemy
        self.__my_last_move_history[Constants.BOARD_VALUE] = board_value

    # return the a sinmple point for this board
    def board_value(self, board: Board):
        normal_me = 0
        king_me = 0
        normal_enemy = 0
        king_enemy = 0
        will_die_me = self.num_will_die_my_pices(board)
        will_die_enemy = self.num_will_die_my_enemy(board)

        for i in range(8):
            for j in range(8):
                cell = board.cells[i][j]
                if not cell.isActive:
                    continue
                if cell.get_player_color() == self.__color:
                    if cell.isKing():
                        king_me += 1
                    else:
                        normal_me += 1
                elif cell.get_player_color() != Constants.UNKNOWN:
                    if cell.isKing():
                        king_enemy += 1
                    else:
                        normal_enemy += 1

        board_value = self.w0 + self.w1 * normal_me + self.w2 * normal_enemy + self.w3 * king_me + self.w4 * king_enemy + self.w5 * will_die_me + self.w6 * will_die_enemy
        return board_value

    def get_best_move_from_dict(self, dict_move_value: dict, ) -> str:
        if len(dict_move_value.keys()) == 0:
            return "NN"

        sorted_dict = sorted(dict_move_value.items(), key=lambda kv: (kv[1], kv[0]),
                             reverse=True)  # sort (big -> small)
        (temp, max) = sorted_dict[0]
        best_list = []
        best_list.append((temp, max))
        for key, value in sorted_dict:
            if key == temp:
                continue
            if value >= max:
                best_list.append((key, value))

        choosen_inex = 0
        if len(best_list) > 1:  # list equal values
            choosen_inex = int(random.random() * len(best_list))
            # print("chosen index :", choosen_inex)

        (move, value) = sorted_dict[choosen_inex]
        return move

    def is_out_of_range(self, y: int, x: int) -> bool:
        return not (0 <= y < 8 and 0 <= x < 8)

    # list neabor cells in yx
    def list_neighbours(self, y: int, x: int) -> [(int, int)]:
        res = []
        for i in [y - 1, y + 1]:
            for j in [x - 1, x + 1]:
                if not self.is_out_of_range(i, j):
                    res.append((i, j))
        return res

    # can cell Y0X0 cross Over Y1X1
    def can_cross_this(self, board: Board, y0, x0, y1, x1) -> bool:
        cell_start = board.cells[y0][x0]
        cell_end = board.cells[y1][x1]
        if not cell_start.isActive or not cell_end.isActive:
            return False  # deActive cell can do nothing
        if cell_start.get_player_color() == cell_end.get_player_color() or cell_end.get_player_color() == Constants.UNKNOWN:
            return False  # cannot jump over our self! OR Empty cell
        if abs(y0 - y1) != 1 or abs(x0 - x1) != 1:
            return False  # they are not neabor
        if y1 == y0 + 1 and x1 == x0 + 1:  # ->^
            yy = y1 + 1
            xx = x1 + 1
            return self.is_good_cell(board, yy, xx)
        elif y1 == y0 + 1 and x1 == x0 - 1:  # <-^
            yy = y1 + 1
            xx = x1 - 1
            return self.is_good_cell(board, yy, xx)
        elif y1 == y0 - 1 and x1 == x0 + 1:  # ->V
            yy = y1 - 1
            xx = x1 + 1
            return self.is_good_cell(board, yy, xx)
        elif y1 == y0 - 1 and x1 == x0 - 1:  # <-V
            yy = y1 - 1
            xx = x1 - 1
            return self.is_good_cell(board, yy, xx)

        return False

    # cell is in Board & isEmpty & usable!
    def is_good_cell(self, board: Board, yy: int, xx: int) -> bool:
        if self.is_out_of_range(yy, xx):
            return False
        cell_cross = board.cells[yy][xx]
        return cell_cross.get_player_color() == Constants.UNKNOWN and cell_cross.isActive

    # base on enemy pices ....
    def num_will_die_my_pices(self, board: Board) -> int:
        count = 0
        for i in range(8):
            for j in range(8):
                cell = board.cells[i][j]
                if not cell.isActive or cell.get_player_color() == self.__color or cell.get_player_color() == Constants.UNKNOWN:
                    continue
                neighbours = self.list_neighbours(i, j)
                for yy, xx in neighbours:
                    if self.can_cross_this(board, i, j, yy, xx):
                        count += 1
        return count

    # base on my picess
    def num_will_die_my_enemy(self, board: Board) -> int:
        count = 0
        for i in range(8):
            for j in range(8):
                cell = board.cells[i][j]
                if not cell.isActive or cell.get_player_color() != self.__color or cell.get_player_color() == Constants.UNKNOWN:
                    continue
                neighbours = self.list_neighbours(i, j)
                for yy, xx in neighbours:
                    if self.can_cross_this(board, i, j, yy, xx):
                        count += 1
        return count

    # print and return stats !
    def print_stats(self) -> dict:
        knowledge = {"w0": self.w0,
                     "w1": self.w1,
                     "w2": self.w2,
                     "w3": self.w3,
                     "w4": self.w4,
                     "w5": self.w5,
                     "w6": self.w6}
        print(knowledge)
        return knowledge

    def error(self, board: Board):
        curent_value = self.board_value(board)
        old_value = self.__my_last_move_history.get(Constants.BOARD_VALUE, 0)
        if curent_value != curent_value:  # current value is Not A Number
            curent_value = 0
        if old_value != old_value:  # current value is Not A Number
            old_value = 0
        if old_value is None:
            old_value = 0

        diff = curent_value - old_value
        # limitiing error values
        if diff < -10:
            diff = -10
        if diff > 10:
            diff = 10
        return diff

    # when enemy moves you have to update what you know ...
    def update_knowledge(self, board: Board):
        if not self.is_Train_mod:  # do not update weights if not train mod
            return

        normal_me = self.__my_last_move_history.get(Constants.normal_me, 0)
        king_me = self.__my_last_move_history.get(Constants.king_me, 0)
        normal_enemy = self.__my_last_move_history.get(Constants.normal_enemy, 0)
        king_enemy = self.__my_last_move_history.get(Constants.king_enemy, 0)
        will_die_me = self.__my_last_move_history.get(Constants.will_die_me, 0)
        will_die_enemy = self.__my_last_move_history.get(Constants.will_die_enemy, 0)

        error = self.error(board)  # error in calculating condition
        # save_on_file_log("ERROR : " + str(error))
        self.w0 = self.w0 + (self.__neta * error)
        self.w1 = self.w1 + (self.__neta * (error * normal_me))
        self.w2 = self.w2 + (self.__neta * (error * normal_enemy))
        self.w3 = self.w3 + (self.__neta * (error * king_me))
        self.w4 = self.w4 + (self.__neta * (error * king_enemy))
        self.w5 = self.w5 + (self.__neta * (error * will_die_me))
        self.w6 = self.w6 + (self.__neta * (error * will_die_enemy))

        knowledge = {"w0": self.w0,
                     "w1": self.w1,
                     "w2": self.w2,
                     "w3": self.w3,
                     "w4": self.w4,
                     "w5": self.w5,
                     "w6": self.w6}

        values = {
            "normal_me": normal_me,
            "king_me": king_me,
            "normal_enemy": normal_enemy,
            "king_enemy": king_enemy,
            "will_die_me": will_die_me,
            "will_die_enemy": will_die_enemy}
        # save_on_file_log("values : " + str(values))
        self.save_board_values(board)

        save_on_file_log(self.__name + " : " + (str(knowledge)))
        # These values are bad ! after 200 iteration will get to -inf or +inf
        # self.w0 = self.w0 + (self.__neta * error * 1)
        # self.w1 = self.w1 + (self.__neta * error * normal_me)
        # self.w2 = self.w2 + (self.__neta * error * normal_enemy)
        # self.w3 = self.w3 + (self.__neta * error * king_me)
        # self.w4 = self.w4 + (self.__neta * error * king_enemy)
        # self.w5 = self.w5 + (self.__neta * error * will_die_me)
        # self.w6 = self.w6 + (self.__neta * error * will_die_enemy)
