"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2019
morfi dose'nt like copy ! [ so it's different !!]
"""

# constant yes no in CSV !!
STR_NO = "Do Not Enjoy"
STR_YES = "Enjoy Sport"


class Data:
    def isPositive(self) -> bool:
        return self.__valid

    def __init__(self, list_data, valid: bool):
        self.__valid = valid
        self.sub_data = list_data

    def __str__(self) -> str:
        if self.__valid:
            return str(self.sub_data) + " - " + "TRUE"
        else:
            return str(self.sub_data) + " - " + "FALSE"


class CandidElemenate:
    G = []
    S = []
    vs = []
    sizes = []

    data = []
    has_no_answer = False

    """range of input value !
        EX: [ 3 , 5 , 4 ,2 ,1]
    """

    def __init__(self, data_range):
        self.sizes = data_range
        self.G.clear()
        self.S.clear()
        small_g = []
        for i in range(len(data_range)):
            small_g.append("?")
            self.S.append("0")
        self.G.append(small_g)

    # def get_data(self) -> [Data]:
    #     return []

    def set_train_data(self, data):
        self.data = data

    def do_the_thing(self):

        # data = self.data
        for line in self.data:
            if line.isPositive():  # positive

                # at least one acceptable g should be in G ?
                has_one_answer = False
                for g in self.G:  # the Clues Woudln't satisfy with current G so-> There is Real Problem
                    if not self.is_accepted(line.sub_data, g):
                        continue
                    else:
                        has_one_answer = True
                if not has_one_answer:
                    self.has_no_answer = True
                    return

                for index in range(len(self.sizes)):
                    s = self.S[index]
                    d = line.sub_data[index]
                    if s == "?" or s == d:
                        continue
                    if s == "0":
                        self.S[index] = d
                    else:
                        self.S[index] = "?"

                delete_list = []
                for index in range(len(self.G)):
                    gg = self.G[index]
                    if not self.is_accepted(line.sub_data, gg):
                        delete_list.append(index)
                    # self.G.append(g_item)
                delete_list.sort(reverse=True)
                for item in delete_list:
                    if len(self.G) != 0 and len(self.G) > item:
                        self.G.pop(item)  # item = index

            else:  # negative data input
                # self.remove_incorrect_spec()
                # re generalize g -------------------
                if self.is_accepted(line.sub_data, self.S):
                    self.has_no_answer = True
                    return
                    # return "0"  # Conflict with S means -> There is no Answer

                will_add_to_g_set = []
                for index in range(len(self.sizes)):
                    s = self.S[index]
                    d = line.sub_data[index]
                    for j in range(len(self.G)):
                        g = self.G[j][index]
                        if g == d:  # g alredy contain this
                            continue
                        if s != d:  # value in s is Diffrence
                            new_set = self.build_general_set(index, [s])
                            will_add_to_g_set.append(new_set)
                        elif s == d:
                            continue
                delete_list = []
                for g_item in will_add_to_g_set:
                    for index in range(len(self.G)):
                        gg = self.G[index]
                        if self.is_more_general(gg, g_item):
                            delete_list.append(index)
                    # self.G.append(g_item)
                delete_list.sort(reverse=True)
                for item in delete_list:
                    if len(self.G) != 0 and len(self.G) > item:
                        self.G.pop(item)  # item = index
                for g_item in will_add_to_g_set:
                    self.G.append(g_item)

                self.remove_redundant_from_g()
                self.make_least_general_g()

        print("S :", self.S)
        print("G :", self.G)

    """ Empty s = [0 , 0 , .... 0 ]"""

    def empty_s(self):
        for i in range(len(self.S)):
            self.S[i] = "0"

    def build_general_set(self, start_index: int, values) -> []:
        tempt = []
        index_in_value = 0
        list_size = len(values)
        for i in range(len(self.sizes)):
            if i < start_index:
                tempt.append("?")
            elif index_in_value >= list_size:
                tempt.append("?")
            else:
                tempt.append(values[index_in_value])
                index_in_value += 1
        return tempt

    def is_more_general(self, list1, list2):
        size1 = len(list1)
        size2 = len(list2)
        question_1 = 0
        question_2 = 0
        if size1 != size2:
            return False
        if size1 == 0:
            return False
        for index in range(size1):
            item1 = list1[index]
            item2 = list2[index]
            if item1 == "?":
                question_1 += 1
            if item2 == "?":
                question_2 += 1
        return question_1 > question_2

    def remove_redundant_from_g(self):
        rem_list = []
        for i in range(len(self.G) - 1):
            for j in range(i + 1, len(self.G)):
                if self.G[i] == self.G[j]:
                    rem_list.append(j)

        rem_list.sort(reverse=True)
        rem_list = list(dict.fromkeys(rem_list))
        for index in rem_list:
            self.G.pop(index)

    def make_least_general_g(self):
        rem_list = []
        for i in range(len(self.G) - 1):
            for j in range(i + 1, len(self.G)):
                if self.is_more_general(self.G[i], self.G[j]):
                    rem_list.append(i)

        # checking last index with firts index [ normally lat index wont be checked!]
        if len(self.G) - 1 != 0 and self.is_more_general(self.G[len(self.G) - 1], self.G[0]):
            rem_list.append(len(self.G) - 1)

        rem_list.sort(reverse=True)
        rem_list = list(dict.fromkeys(rem_list))
        for index in rem_list:
            self.G.pop(index)

    """Is given clues acceptable in this hypo ?"""

    def is_accepted(self, cluse: list, hypo: list):
        for i in range(len(hypo)):
            hi = hypo[i]
            ci = cluse[i]
            if hi == "?" or hi == ci:
                continue
            else:
                return False
        return True

    """:return list of hypos that are True """

    def build_version_spave(self) -> []:
        self.vs.append(self.S)
        for g in self.G:
            self.vs.append(g)
        return self.vs  # Vs Whold be Some Where There!

    def get_vs(self):
        return self.vs


"""check single hype validity"""


def is_accepptable(row_value: [], hypo: []) -> bool:
    for i in range(len(hypo)):
        hi = hypo[i]
        ri = row_value[i]
        if hi == "?" or hi == ri:
            continue
        if hi != ri:
            return False
    return True


""":returns if Row in csv would be satisfied with our VS
this Section is YES Oriented [ prefers saying YES rather than no ... ]
it works statistical !
"""


def is_row_acceptable(row_value: [], VersionSpace: [], ) -> bool:
    accept_count = 0
    reject_count = 0
    for hy in VersionSpace:
        if is_accepptable(row_value, hy):
            accept_count += 1
        else:
            reject_count += 1
    return accept_count >= reject_count


def use_testing_data_set(vs: []):
    import csv
    with open('data_set.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        false_neg_count = 0
        false_poss_count = 0
        true_negative_count = 0
        true_poss_count = 0

        for row in csv_reader:
            if line_count == 0:
                line_count += 1
                continue
            else:
                if row[6] == STR_YES:
                    datta = Data([row[0], row[1], row[2], row[3], row[4], row[5]], True)
                    res = is_row_acceptable(datta.sub_data, vs)
                    if res:
                        true_poss_count += 1
                        # print("TP:", datta)
                    else:
                        false_neg_count += 1
                        # print("FN:", datta)
                else:
                    datta = Data([row[0], row[1], row[2], row[3], row[4], row[5]], False)
                    res = is_row_acceptable(datta.sub_data, vs)
                    if res:
                        false_poss_count += 1
                        # print("FP:", datta)
                    else:
                        true_negative_count += 1
                        # print("TN:", datta)
                line_count += 1
        print("TP:", true_poss_count, "TN:", true_negative_count, "FP:", false_poss_count, "FN:", false_neg_count,
              "TOTAL:",
              line_count - 1)


# mian part start here --------------------
data = [
    Data(["Sunny", "Warm", "Normal", "Strong", "Warm", "Same"], True),
    Data(["Sunny", "Warm", "High", "Strong", "Warm", "Same"], True),
    Data(["Rainy", "Cold", "High", "Strong", "Warm", "Change"], False),
    Data(["Sunny", "Warm", "High", "Strong", "Cool", "Change"], True),

]
cd = CandidElemenate([3, 2, 2, 2, 2, 2])
cd.set_train_data(data)
cd.do_the_thing()
if cd.has_no_answer:
    print(cd.S)
    print("THERE IS NO ANSWER BASED ON TRAIN SET")
else:
    cd.build_version_spave()
    use_testing_data_set(cd.get_vs())
