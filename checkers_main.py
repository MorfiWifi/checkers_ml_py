"""
Created by MorfiWifi AKA morteza eydipour (9811634)
2019 - KNT Uni

-- First real python
FOR better seen using blue instead of black
Command Are like e7h7 a1b3
Highly recommended Use WINDWOS_CMD = False buit you have 
to use ide command line (terminal) or non windwo! to see Tru Colors!
other you are like color bline man! can't see wich is your pice
"""
import os
import aiAgent as Ai
from game_pices import Board, Cell
from utill import WINDWOS_CMD, alpha_num, Constants , save_on_file_log
from judge import Judge

# def error(board:board , side ):
#     return 0

# list of ai agents ...........
list_ai = []


def clear_consol():
    # pass
    # try:
    # os.system('clear')  # For Linux/OS
    os.system('cls')  # For Windows


# save latest Learning values on file ....
def save_on_file(ais: [Ai]):
    f = open("checkers_saves.txt", "w")
    for ai in ais:
        ai_color = ai.getColor()
        f.write("Ai Agent:" + "\n")
        f.write(ai_color + "\n")
        dict_knowledge = ai.get_agent_knowledge()
        for key in dict_knowledge.keys():
            value = dict_knowledge[key]
            if value != value:
                print("KEY:", key, "FOR:", ai_color, "was null")
            f.write(key + ":" + str(value) + "\n")
    f.flush()
    f.close()





# load learning values from fiel .....
def load_from_file(ais: [Ai]):
    try:
        f = open("checkers_saves.txt", "r")
    except:
        print("Could not Find Save File")
        return
    index = 0
    mess = f.readline()
    if mess != ("Ai Agent:" + "\n"):
        return  # file dosent satr with correct data
    mess = f.readline()

    lines = f.readlines()
    if index > 1:
        index = 1
    for mess in lines:
        # while mess is not None:
        if mess == ("Ai Agent:" + "\n"):
            index += 1
            continue  # ignore this
        if mess.count(":") == 0:
            continue  # ignore this
        if index > 1:
            index = 1
        ai = ais[index]
        splited = mess.split(":")
        agent = ais[index]
        agent.set_data_in_knowldege(splited[0], float(splited[1]))
        if splited[0] == "w0":
            ai.w0 = float(splited[1])
        if splited[0] == "w1":
            ai.w1 = float(splited[1])
        if splited[0] == "w2":
            ai.w2 = float(splited[1])
        if splited[0] == "w3":
            ai.w3 = float(splited[1])
        if splited[0] == "w4":
            ai.w4 = float(splited[1])
        if splited[0] == "w5":
            ai.w5 = float(splited[1])
        if splited[0] == "w6":
            ai.w6 = float(splited[1])

    f.close()


class TEXTCOLOR:
    PINK = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    LIGHT_BLUE = '\033[96m'
    BLACK = '\033[97m'
    WHITE = '\033[98m'
    HOLLOWED = '\033[100m'
    HOLLOW_ORANGE = '\033[101m'
    GRAY = '\033[90m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# this wont go next line be care full
def print_colory(data, text_color):
    if WINDWOS_CMD:
        print(data, end="")
    else:
        print(text_color, data, TEXTCOLOR.ENDC, end="")


def print_board(board: Board):
    for i in range(7, -1, -1):  # this is y
        print(i + 1, ": |", end="")
        for j in range(8):  # x from 0
            celi = board.cells[i][j]
            if celi.isActive:
                if celi.get_player_color() == Constants.RED:
                    print_colory(celi.get_char(), TEXTCOLOR.RED)
                elif celi.get_player_color() == Constants.BLACK:
                    print_colory(celi.get_char(), TEXTCOLOR.BLUE)
                else:
                    print_colory("  ", TEXTCOLOR.BLACK)
            else:  # cell is WHITE -> can't use it
                print_colory("##", TEXTCOLOR.WHITE)

            if j == 7:
                print("|")
            else:
                print("|", end="")
    if WINDWOS_CMD:
        print("& --| a| b| c| d| e| f| g| h| ")
    else:
        print("& --| a  | b  | c  | d  | e  | f  | g  | h  | ")


# main part -----------------------------------------
# load_from_file()
# breakpoint()


print("Input Game Mod : (Only the number ex: 2)")
print("1.Training (Two Same Ai) 2000 reps (No load)")
print("2.Training (Two +- 0.5  Ai diff) 2000 rep (No Load)")
print("3.Training (Two) 2000 rep (Load From File)")
print("4.Ai Vs Ai  (load ai from file)")
print("5.Ai Vs Man (man in red) (load ai from file)")
print("-else Exit-")

inn = input("Choose : ")
if inn == "1":

    # agent1.random_initialize_weights()
    # agent1.save_board_values(board)
    # agent2.save_board_values(board)
    # load_from_file([agent1, agent2])
    # save_on_file([agent1, agent2])
    # save_on_file(agent2)
    # save_on_file_log("morfi messae 1 : god dose'nt exist2")

    # breakpoint()

    for i in range(2000):
        board = Board()
        board.init_empty()
        print_board(board)
        judge = Judge()

        agent1 = Ai.Ai("red_player", False, Constants.RED)
        agent2 = Ai.Ai("black_player", False, Constants.BLACK)
        list_ai.append(agent1)
        list_ai.append(agent2)

        # while game is not finished play ...
        winner_color = judge.isGameFinished(board=board)

        while winner_color == Constants.UNKNOWN:
            turn = judge.get_turn()
            for index in [0, 1]:
                if list_ai[index].getColor() == turn:
                    list_ai[index].next_moove(board, judge)
                    judge.move_don()
                    clear_consol()
                    print_board(board)

                    if judge.get_move_count() > 1:
                        if index == 0:
                            list_ai[1].update_knowledge(board)
                            # save_on_file(list_ai)
                        else:
                            list_ai[0].update_knowledge(board)
                            # save_on_file(list_ai)

                    break
            winner_color = judge.isGameFinished(board)
            if judge.get_move_count() > 500:
                winner_color = Constants.DRAW  # game shouldnt take that long
        else:
            if winner_color == Constants.DRAW:
                save_on_file_log("game num: " + str(i) + " MOVES: " + str(judge.get_move_count()) + " DRAW")
                save_on_file([agent1, agent2])
                print("DRAW")
                continue
            for ai in list_ai:
                if ai.getColor() == winner_color:
                    print("winner is " + ai.getName())
                    save_on_file_log(
                        "game num: " + str(i) + " MOVES: " + str(judge.get_move_count()) + " winner is " + ai.getName())
                    save_on_file([agent1, agent2])
                    continue


elif inn == "2":
    for i in range(2000):
        board = Board()
        board.init_empty()
        print_board(board)
        judge = Judge()

        agent1 = Ai.Ai("red_player", False, Constants.RED)
        agent2 = Ai.Ai("black_player", False, Constants.BLACK)
        agent2.random_initialize_weights()
        agent1.random_initialize_weights()

        list_ai.append(agent1)
        list_ai.append(agent2)

        # while game is not finished play ...
        winner_color = judge.isGameFinished(board=board)

        while winner_color == Constants.UNKNOWN:
            turn = judge.get_turn()
            for index in [0, 1]:
                if list_ai[index].getColor() == turn:
                    list_ai[index].next_moove(board, judge)
                    judge.move_don()
                    clear_consol()
                    print_board(board)

                    if judge.get_move_count() > 1:
                        if index == 0:
                            list_ai[1].update_knowledge(board)
                        else:
                            list_ai[0].update_knowledge(board)

                    break
            winner_color = judge.isGameFinished(board)
            if judge.get_move_count() > 500:
                winner_color = Constants.DRAW  # game shouldnt take that long
        else:
            if winner_color == Constants.DRAW:
                save_on_file_log("game num: " + str(i) + " MOVES: " + str(judge.get_move_count()) + " DRAW")
                save_on_file([agent1, agent2])
                print("DRAW")
                continue
            for ai in list_ai:
                if ai.getColor() == winner_color:
                    print("winner is " + ai.getName())
                    save_on_file_log(
                        "game num: " + str(i) + " MOVES: " + str(judge.get_move_count()) + " winner is " + ai.getName())
                    save_on_file([agent1, agent2])
                    continue
elif inn == "3":
    agent1 = Ai.Ai("red_player", False, Constants.RED)
    agent2 = Ai.Ai("black_player", False, Constants.BLACK)
    agent2.random_initialize_weights()
    agent1.random_initialize_weights()
    load_from_file([agent1, agent2])
    list_ai.append(agent1)
    list_ai.append(agent2)

    for i in range(2000):
        board = Board()
        board.init_empty()
        print_board(board)
        judge = Judge()

        # while game is not finished play ...
        winner_color = judge.isGameFinished(board=board)

        while winner_color == Constants.UNKNOWN:
            turn = judge.get_turn()
            for index in [0, 1]:
                if list_ai[index].getColor() == turn:
                    list_ai[index].save_board_values(board)
                    list_ai[index].next_moove(board, judge)
                    judge.move_don()
                    clear_consol()
                    print_board(board)

                    if judge.get_move_count() > 1:
                        if index == 0:
                            list_ai[1].update_knowledge(board)
                            save_on_file(list_ai)
                        else:
                            list_ai[0].update_knowledge(board)
                            save_on_file(list_ai)

                    break
            winner_color = judge.isGameFinished(board)
            if judge.get_move_count() > 500:
                winner_color = Constants.DRAW  # game shouldnt take that long
        else:
            if winner_color == Constants.DRAW:
                save_on_file_log("game num: " + str(i) + " MOVES: " + str(judge.get_move_count()) + " DRAW")
                save_on_file([agent1, agent2])
                print("DRAW")
                continue
            for ai in list_ai:
                if ai.getColor() == winner_color:
                    print("winner is " + ai.getName())
                    save_on_file_log(
                        "game num: " + str(i) + " MOVES: " + str(judge.get_move_count()) + " winner is " + ai.getName())
                    save_on_file([agent1, agent2])
                    ai.print_stats()
                    continue
elif inn == "4":
    board = Board()
    board.init_empty()
    print_board(board)
    judge = Judge()

    agent1 = Ai.Ai("red_player", False, Constants.RED)
    agent2 = Ai.Ai("black_player", False, Constants.BLACK)
    load_from_file([agent1, agent2])
    agent1.is_Train_mod = False
    agent2.is_Train_mod = False

    list_ai.append(agent1)
    list_ai.append(agent2)
    # while game is not finished play ...
    winner_color = judge.isGameFinished(board=board)

    while winner_color == Constants.UNKNOWN:
        turn = judge.get_turn()
        for index in [0, 1]:
            if list_ai[index].getColor() == turn:
                list_ai[index].next_moove(board, judge)
                judge.move_don()
                clear_consol()
                print_board(board)

                if judge.get_move_count() > 1:
                    if index == 0:
                        list_ai[1].update_knowledge(board)
                        save_on_file(list_ai)
                    else:
                        list_ai[0].update_knowledge(board)
                        save_on_file(list_ai)

        winner_color = judge.isGameFinished(board)
        if judge.get_move_count() > 500:
            winner_color = Constants.DRAW  # game shouldnt take that long
    else:
        if winner_color == Constants.DRAW:
            save_on_file_log("game num: " + str(judge.get_move_count()) + " DRAW")
            print("DRAW")

        for ai in list_ai:
            if ai.getColor() == winner_color:
                print("winner is " + ai.getName())
                save_on_file_log(
                    "Ai NO TRAIN" + " MOVES: " + str(judge.get_move_count()) + " winner is " + ai.getName())


elif inn == "5":
    board = Board()
    board.init_empty()
    print_board(board)
    judge = Judge()

    inn = input("WICH COLOR YOU LIKE 1.Black 2.Red (ai color load's from file) : ")
    if inn == "1":
        agent1 = Ai.Ai("red_player", False, Constants.RED)
        agent2 = Ai.Ai("black_player", True, Constants.BLACK)
    else:
        agent1 = Ai.Ai("red_player", True, Constants.RED)
        agent2 = Ai.Ai("black_player", False, Constants.BLACK)

    # this game is not for training
    agent1.is_Train_mod = False
    agent2.is_Train_mod = False

    list_ai.append(agent1)
    list_ai.append(agent2)
    load_from_file([agent1, agent2])
    # while game is not finished play ...
    winner_color = judge.isGameFinished(board=board)
    while winner_color == Constants.UNKNOWN:
        turn = judge.get_turn()
        for index in [0, 1]:
            if list_ai[index].getColor() == turn:
                list_ai[index].next_moove(board, judge)
                judge.move_don()
                clear_consol()
                print_board(board)

                if judge.get_move_count() > 1:
                    if index == 0:
                        list_ai[1].update_knowledge(board)
                        save_on_file(list_ai)
                    else:
                        list_ai[0].update_knowledge(board)
                        save_on_file(list_ai)

        winner_color = judge.isGameFinished(board)
        if judge.get_move_count() > 500:
            winner_color = Constants.DRAW  # game shouldnt take that long
    else:
        if winner_color == Constants.DRAW:
            save_on_file_log("game num: " + str(judge.get_move_count()) + " DRAW")
            save_on_file([agent1, agent2])
            print("DRAW")

        for ai in list_ai:
            if ai.getColor() == winner_color:
                print("winner is " + ai.getName())
                save_on_file_log(
                    "HUMAN VS AI : " + " MOVES: " + str(judge.get_move_count()) + " winner is " + ai.getName())
                if inn == "1":
                    save_on_file_log("HUMAN was BLACK")
                else:
                    save_on_file_log("HUMAN was RED")
                save_on_file([agent1, agent2])
else:
    print("GOOD BYE")


# board = Board()
# board.init_empty()
# print_board(board)
# judge = Judge()
#
# agent1 = Ai.Ai("red_player", False, Constants.RED)
# agent2 = Ai.Ai("black_player", False, Constants.BLACK)

# agent1.random_initialize_weights()
# agent1.save_board_values(board)
# agent2.save_board_values(board)
# load_from_file([agent1, agent2])
# save_on_file([agent1, agent2])
# save_on_file(agent2)
# save_on_file_log("morfi messae 1 : god dose'nt exist2")

# breakpoint()
#
# list_ai.append(agent1)
# list_ai.append(agent2)
#
# # while game is not finished play ...
# winner_color = judge.isGameFinished(board=board)
#
# while winner_color == Constants.UNKNOWN:
#     turn = judge.get_turn()
#
#     for ai in list_ai:
#         if ai.getColor() == turn:
#             ai.next_moove(board, judge)
#             judge.move_don()
#             clear_consol()
#             print_board(board)
#             break
#
#     # for ai in list_ai:
#     #     ai.update_knowledge(board)
#     # judge.Turn_manager = tunr_manager
#
#     winner_color = judge.isGameFinished(board)
#     if judge.is_draw():
#         print("--- Match was Draw ---")
#         break
# else:
#     for ai in list_ai:
#         if ai.getColor() == winner_color:
#             print("winner is " + ai.getName())
#             break
#     input("Press Enter To Exit")


# test Function -------------------------------------------
def remove_cell():
    # input where -------------------
    command = input("Write your moove : ")
    while command != "00":
        if len(command) < 2:
            print("NON valid commadn!")
        else:
            x0 = alpha_num.get(command[0])
            y0 = int(command[1]) - 1
            scc = board.cells[y0][x0]  # just disable cella3
            scc.isActive = False
            print_board(board)
            print("Board Updated")
        command = input("00 TO EXIT : ")
