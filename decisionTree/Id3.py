"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2019
morfi dose'nt like copy ! [ so it's different !!]
out put values are normally bit mod (true , false)
Monk DataSet Attribute information:
    1. class: 0, 1
    2. a1:    1, 2, 3
    3. a2:    1, 2, 3
    4. a3:    1, 2
    5. a4:    1, 2, 3
    6. a5:    1, 2, 3, 4
    7. a6:    1, 2
    8. Id:    (A unique symbol for each instance)
** this Implementation is bad For memory!
** but more Easy to Work with in Code!!
"""
import math, copy

# static Values ---------------------------------
UNKNOWN = "unknown"
NO = "0"  # validity based on Monk DTS
YES = "1"
DEFAULT_LABLE = NO
VALIDITY = "validity"
LEARN_SET_CSV_PATH = "monks-1test.csv"  # this is full data base!!
LEARN_SET_SIZE = 432  # size of data
TRAIN_SET_CSV_PATH = "monks-1train.csv"
global INDEX
INDEX = 0  # Jus a simple Identifier


# static Values ---------------------------------

# this always Increases Unique Value! [for finding Node!]
def get_identifier() -> int:
    global INDEX
    INDEX += 1
    return INDEX


"""Full Atribute values """


class AttributeType:
    values = []
    name = UNKNOWN

    def __init__(self, name, values: []):
        self.values = values
        self.name = name


class Node:
    def __init__(self, data_set: [], attributes: []):
        self.s = data_set
        self.atts = attributes
        self.next = []  # this is a node list
        self.edges = []  # list of values edges
        self.name = ""  # str
        self.value = ""  # this is for Leafs
        # self.s = []  # data set in this node
        # self.atts = []  # limited attribures hass acess to
        self.__is_leafe = True
        self.__lable = "UNKNOWN"
        self.__has_processed = False
        self.id = get_identifier()

    def is_processed(self) -> bool:
        return self.__has_processed

    def get_lable(self) -> str:
        return self.__lable

    def isLeafe(self) -> bool:
        return self.__is_leafe

    # Modified Version !!!
    def doo_calculateions(self):
        examps_size = len(self.s)
        att_size = len(self.atts)
        if examps_size == 0:
            self.__has_processed = True
            self.__lable = DEFAULT_LABLE
            self.__is_leafe = True
            return []  # return Default [PeSudo Code]

        # chek same Class -> Y: Leafe! N: Check thing! -----------------------------------
        all_same_class = True
        default_clss = self.s[0][forward_map[VALIDITY]]
        for line in self.s:
            if line[forward_map[VALIDITY]] != default_clss:
                all_same_class = False

        if all_same_class:
            self.__has_processed = True
            self.__is_leafe = True
            self.__lable = default_clss
            return []  # return From This Sub tree

        # Statestical Choose the Leave lable! ---------------------------------------
        if att_size <= 1:  # no Extra Att -> this is Leave
            self.__is_leafe = True
            class_count = {}
            for cc in classes:
                class_count.setdefault(cc, 0)

            for linei in self.s:
                for cc in classes:
                    if linei[forward_map[VALIDITY]] == cc:
                        class_count[cc] += 1

            most_count = -math.inf
            for cc in classes:
                if class_count[cc] >= most_count:
                    default_clss = cc
                    most_count = class_count[cc]

            self.__lable = default_clss
            self.__has_processed = True
            self.__is_leafe = True
            return []  # return from this sub tree

        # if you came here we have Sub Tree To Handel ---------------------------
        # BUILD SUB TREE
        # calculate Gain
        self.__is_leafe = False  # this is A Node
        max_gain = -math.inf
        atr_name = "NON"
        for att_t in self.atts:
            value = info_gain(self.s, att_t.name)
            if value > max_gain:
                max_gain = value
                atr_name = att_t.name

        self.name = atr_name
        self.edges = AttributeTypes[forward_map[atr_name] - 1].values

        next_nodes = []
        for value in self.edges:
            ds = []  # sub Tress Data Set
            atts = []  # sub Tree Attributes

            for att in self.atts:
                if att.name != atr_name:
                    atts.append(att)

            for line in leanData:
                if line[forward_map[atr_name]] == value:
                    ds.append(line)

            # self.
            # sub_tree = self(ds, atts)
            # sub_tree.__init__(ds, atts)
            # sub_tree.doo_calculateions()
            # self.next.append(sub_tree)
            next_nodes.append((ds, atts))
        return next_nodes  # 1 for Non Leave
        # end of Building Tree --------------------------------------------------


class Recursive_hlper_class():
    # IDon't Know Python!
    # As I See It Dose'nt Support Recursive
    # as I Know it in Java , C# , C
    # Some How self Conflict's With Parent self!
    # so I Have To Handle Recursive Throw
    # Middle Class !!
    process_list = []  # should provess node list

    # initialize with Root
    def __init__(self, root: Node):
        self.root = root
        self.__doo_calculateions(self.root)

    # prefered for Root Only !
    def __doo_calculateions(self, node: Node):
        answers = node.doo_calculateions()
        count_sub_nodes = len(answers)
        if count_sub_nodes == 0:
            self.__nod_is_finished(node)
            return 0  # it was leave
        else:
            for (ds, atts) in answers:
                nn = Node(ds, atts)
                node.next.append(nn)
                self.process_list.append(nn)
            self.__nod_is_finished(node)

    def __nod_is_finished(self, nod: Node):
        self.__remove_redundency([nod])

    def __remove_redundency(self, nodes: []):
        for noodi in nodes:
            id = noodi.id
            rem_index = -1
            for i in range(len(self.process_list)):
                nod = self.process_list[i]
                if nod.id == id:
                    rem_index = i
                    break
            if rem_index != -1:
                self.process_list.pop(rem_index)

    def should_dig(self) -> bool:
        return len(self.process_list) > 0

    def calculate(self):
        if len(self.process_list) == 0:
            print("NO NOD To Process")
            return 0  # nothing to Work ON
        node = self.process_list[0]
        self.__doo_calculateions(node)


# global Property for App -------------------------
AttributeTypes = []
leanData = []  # simple array for CSV File
trainData = []  # simple array for CSV File
revers_map = {0: VALIDITY, 1: "a1", 2: "a2", 3: "a3", 4: "a4", 5: "a5", 6: "a6", 7: "id"}
forward_map = {VALIDITY: 0, "a1": 1, "a2": 2, "a3": 3, "a4": 4, "a5": 5, "a6": 6, "id": 7}
classes = [NO, YES]  # Classified Data With classes
# global Property for App -------------------------

"""initialize data set meta data !"""


def initialize():
    AttributeTypes.append(AttributeType("a1", ["1", "2", "3"]))  # 0
    AttributeTypes.append(AttributeType("a2", ["1", "2", "3"]))  # 1
    AttributeTypes.append(AttributeType("a3", ["1", "2"]))  # 2
    AttributeTypes.append(AttributeType("a4", ["1", "2", "3"]))  # 3
    AttributeTypes.append(AttributeType("a5", ["1", "2", "3", "4"]))  # 4
    AttributeTypes.append(AttributeType("a6", ["1", "2"]))  # 5


def test_id3(root: Node):
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    temp_nod = root
    for line in trainData:
        while not temp_nod.isLeafe():
            atr_name = temp_nod.name
            line_atr_val = line[forward_map[atr_name]]
            for e, nod in zip(temp_nod.edges, temp_nod.next):
                if e == line_atr_val:
                    temp_nod = nod
                    break

        if line[forward_map[VALIDITY]] == temp_nod.get_lable() and temp_nod.get_lable() == YES:
            tp += 1
        elif line[forward_map[VALIDITY]] == temp_nod.get_lable() and temp_nod.get_lable() == NO:
            tn += 1
        elif line[forward_map[VALIDITY]] != temp_nod.get_lable() and temp_nod.get_lable() == YES:
            fp += 1
        elif line[forward_map[VALIDITY]] != temp_nod.get_lable() and temp_nod.get_lable() == NO:
            fn += 1

        temp_nod = root

    return {"TP": tp,
            "TN": tn,
            "FN": fn,
            "FP": fp}


# simple entropy on Entire data set
def entropy(data_set) -> float:
    # this is just + - classified [binary|boolean]
    entro = 0
    size = len(data_set)
    if size == 0:
        return 0  # worth thing could happen!
    map_lable_count = dict()
    for lable in classes:
        map_lable_count[lable] = 0

    for lable in classes:
        for line in data_set:
            if line[0] == lable:
                map_lable_count[lable] += 1

    for key in map_lable_count.keys():
        count = map_lable_count[key]
        point = count / size
        if point == 0:  # this has no effect (error in log)
            continue
        entro += (-1) * (point * math.log(point, 2))

    return entro


def info_gain(s, attribute_name: str) -> float:
    total_entropy = entropy(s)
    att_type_index = forward_map[attribute_name] - 1
    att = AttributeTypes[att_type_index]
    size = len(s)
    att_values_count = dict()  # {str: 0}
    sv = dict()  # {str: []} data set for value!

    for value in att.values:
        att_values_count.setdefault(value, 0)
        sv.setdefault(value, [])  # empty list!
        for line in s:
            if line[forward_map[attribute_name]] == value:
                att_values_count[value] += 1
                sv[value].append(line)

    for key in att_values_count:
        sub_entropy = entropy(sv[key])
        total_entropy -= ((att_values_count[key] / size) * sub_entropy)

    return total_entropy


# read data from CSV and fill Local Data set
def initilaize_learn_set_from_file(percent: float):
    import csv
    # load learn data
    leanData.clear()
    last_line = LEARN_SET_SIZE * percent
    with open(LEARN_SET_CSV_PATH) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count >= last_line:
                continue
            # validity , a1 , ... ,a6 , DataId
            leanData.append([row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]])
            line_count += 1

    # load train file
    trainData.clear()
    with open(LEARN_SET_CSV_PATH) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            # validity , a1 , ... ,a6 , DataId
            trainData.append([row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8]])
            line_count += 1


# main ---------------------------------------
for pr in range(1, 11):
    percent = pr * 0.1
    print("Data set Read:", percent*100, "%")
    initialize()
    initilaize_learn_set_from_file(percent)
    emtro = entropy(leanData)

    root = Node(leanData, AttributeTypes)  # all data & atts
    rch = Recursive_hlper_class(root)
    while rch.should_dig():
        rch.calculate()

    test_res = test_id3(root)

    print(test_res)
