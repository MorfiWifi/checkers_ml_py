"""
Created by MorfiWifi AKA morteza eydipour (9811634)
2019 - KNT Uni
"""
from utill import alpha_num, WINDWOS_CMD, Constants
import copy


# Cell Game contains 8*8 cell array = Board -----------
class Cell:
    x = 0
    y = 0
    color = Constants.UNKNOWN  # Red_norm | black_norm | red_king | black_king | unknown
    isActive = False  # white cells Are use less in Checkers!

    def get_char(self) -> str:
        return self.color

    def get_player_color(self) -> str:
        if self.color == Constants.BLACK_NORMAL or self.color == Constants.BLACK_KING:
            return Constants.BLACK
        elif self.color == Constants.RED_NORMAL or self.color == Constants.RED_KING:
            return Constants.RED
        else:
            return Constants.UNKNOWN

    def set_king(self):
        if self.color == Constants.BLACK_NORMAL or self.color == Constants.BLACK_KING:
            self.color = Constants.BLACK_KING
        else:
            self.color = Constants.RED_KING

    def isKing(self) -> bool:
        if self.color == Constants.BLACK_KING or self.color == Constants.RED_KING:
            return True
        return False

    def clonei(self):
        cell = Cell()
        cell.x = self.x
        cell.y = self.y  # int wont need doop copy I Guess!!
        cell.isActive = copy.deepcopy(self.isActive)
        cell.color = copy.deepcopy(self.color)
        return cell


# this is all the game ! 64 cell containing game pices
class Board:  # 8 row of cells
    cells = [[], [], [], [], [], [], [], []]

    # build an Empty board [red is Down Black(Blue) is UP]
    def init_empty(self):
        for row in self.cells:
            row.clear()

        for i in range(8):  # i is y |
            if i < 3:
                cool = Constants.RED_NORMAL
            elif i > 4:
                cool = Constants.BLACK_NORMAL
            else:
                cool = Constants.UNKNOWN

            for j in range(8):  # j is x --
                if i % 2 != 0:  # odd height
                    isActice = not (j % 2 == 0)  # even x is Usable
                    celi = Cell()
                    celi.isActive = isActice
                    celi.color = cool
                    celi.x = j
                    celi.y = i
                    self.cells[i].append(celi)
                else:
                    isActice = ((j % 2) == 0)
                    celi = Cell()
                    celi.isActive = isActice
                    celi.color = cool
                    celi.x = j
                    celi.y = i
                    self.cells[i].append(celi)

    # As you know deep copy wont work use this instead
    def clonei(self) -> [[], [], [], [], [], [], [], []]:
        # b = Board()
        res = [[], [], [], [], [], [], [], []]
        for i in range(8):
            for j in range(8):
                celi = self.cells[i][j]
                # celli2 = celi.clonei()
                res[i].append(celi.clonei())

        # celi = self.cells[0][0].clonei()
        # celi2 = celi.clonei()

        # celi.color = "SHIT COLOER"

        # print(celi.color)
        # print(celi2.color)
        # return b
        return  res
