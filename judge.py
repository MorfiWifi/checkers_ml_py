"""
Created by MorfiWifi AKA morteza eydipour (9811634)
2019 - KNT Uni

Some one should tell the move is Acceptable or not !
 - user (human) might like to cheat !!

Ai agent won't Do any changes directly {as current arch}
"""

from game_pices import Board, Cell
from utill import Constants, alpha_num


class Judge:
    """ Initialize Function OF HOLY JUDGE !
        All will be don by his snap ....
    """

    __looser_ai_color = Constants.UNKNOWN

    # when ai cannot find move (Jamed) -> he looses the game
    def cant_find_move(self, color: str):
        self.__looser_ai_color = color

    def move_don(self):
        self.__move_count += 1

    def get_move_count(self) -> int:
        return self.__move_count

    def __init__(self):
        self.__turn = Constants.RED
        self.__history = []  # list of moves as history
        self.__move_count = 0
        # print("initiating Judge")

    def add_history(self, move: str):
        if len(self.__history) > 10:
            self.__history = self.__history[0:9]
        self.__history.append(move)

    # uses local history saved in judge instance to consider draw
    def is_draw(self) -> bool:
        if len(self.__history) < 10:
            return False
        if self.__history[0] == self.__history[4] and self.__history[2] == self.__history[6] and self.__history[1] == self.__history[5] and self.__history[3] == self.__history[7]:
            return True
        return False


    # clear history of game
    def clear_history(self):
        self.__history.clear()

    def get_turn(self) -> str:
        return self.__turn

    def change_turn(self):
        if self.__turn == Constants.RED:
            self.__turn = Constants.BLACK
        else:
            self.__turn = Constants.RED
        # print("TURN CHANGED TO : ", self.__turn)

    def list_near_cells(self, y: int, x: int, color: str) -> [(int, int)]:
        res_list = []
        k = 0
        if color == Constants.UNKNOWN:  # king or general cross mod
            for i in [y - 1, y + 1]:
                for j in [x - 1, x + 1]:
                    if 0 <= i < 8 and 0 <= j < 8:
                        res_list.append((i, j))
            return res_list
        elif color == Constants.RED:  # only up is allowed
            k = y + 1
        elif color == Constants.BLACK:
            k = y - 1
        for j in [x - 1, x + 1]:
            if 0 <= k < 8 and 0 <= j < 8:
                res_list.append((k, j))
        return res_list

    def is_usable_cell(self, board: Board, y: int, x: int, color: str) -> bool:
        if 0 <= x < 8 and 0 <= y < 8:
            cell = board.cells[y][x]
            if cell.get_player_color() == Constants.UNKNOWN:
                return True
            return False
        else:
            return False

    # (last_pos_y , last_pos_x , should_remove_y , should_remove_x)
    def list_near_moves(self, board: Board, y: int, x: int, color: str, isKing: bool) -> [(int, int, int, int)]:
        res_list = []
        start_cell = board.cells[y][x]
        if not start_cell.isActive or start_cell.get_player_color() != color or start_cell.color == Constants.UNKNOWN:
            return res_list  # While space cell has no move !!! || Enemy! || emty cell!

        if isKing:
            x_list = [x - 1, x + 1]
            y_list = [y - 1, y + 1]
        elif color == Constants.RED:
            x_list = [x - 1, x + 1]
            y_list = [y + 1]
        else:  # black color
            x_list = [x - 1, x + 1]
            y_list = [y - 1]

        for i in y_list:
            for j in x_list:
                if 0 <= i < 8 and 0 <= j < 8:
                    cell_dest = board.cells[i][j]
                    if cell_dest.get_player_color() == Constants.UNKNOWN:  # empty cell
                        res_list.append((i, j, -1, -1))  # empty cell can go to
                    elif cell_dest.get_player_color() != color:  # enemy is here jump ..
                        if i < y:
                            yyt = i - 1
                        else:
                            yyt = i + 1
                        if j < x:
                            xxt = j - 1
                        else:
                            xxt = j + 1

                        if self.is_usable_cell(board, yyt, xxt, color):  # can jump throw this cell
                            # print("from y: ", y, " x:", x, " -> ", (yyt, xxt, i, j))
                            res_list.append((yyt, xxt, i, j))
        return res_list

    def list_near_moves_start_dest(self, board: Board, y: int, x: int, color: str, isKing: bool) -> [
        (int, int, int, int)]:
        res_list = None
        start_cell = board.cells[y][x]
        if not start_cell.isActive or start_cell.get_player_color() != color or start_cell.color == Constants.UNKNOWN:
            return res_list  # While space cell has no move !!! || Enemy! || emty cell!

        if isKing:
            x_list = [x - 1, x + 1]
            y_list = [y - 1, y + 1]
        elif color == Constants.RED:
            x_list = [x - 1, x + 1]
            y_list = [y + 1]
        else:
            x_list = [x - 1, x + 1]
            y_list = [y - 1]

        for i in y_list:
            for j in x_list:
                if 0 <= i < 8 and 0 <= j < 8:
                    cell_dest = board.cells[i][j]
                    if cell_dest.get_player_color() == Constants.UNKNOWN:
                        if res_list == None:
                            res_list = []
                        res_list.append((y, x, i, j))  # empty cell can go to
                    elif cell_dest.get_player_color() != color:  # enemy is here jump ..
                        if i < y:
                            yyt = i - 1
                        else:
                            yyt = i + 1
                        if j < x:
                            xxt = j - 1
                        else:
                            xxt = j + 1

                        if self.is_usable_cell(board, yyt, xxt, color):  # can jump throw this cell
                            if res_list == None:
                                res_list = []
                            res_list.append((y, x, yyt, xxt))
        return res_list

        # elif color == Constants.RED:  # only up is allowed
        #     k = y + 1
        # elif color == Constants.BLACK:
        #     k = y - 1
        # for j in [x - 1, x + 1]:
        #     if 0 <= k < 8 and 0 <= j < 8:
        #         res_list.append((k, j))
        # return res_list

    # is given move valid with this board
    def isValidMove(self, board: Board, move: str, color: str, checkOnly: bool) -> bool:
        if len(move) < 4:
            # print("AT least 4 char")
            return False  # move is 4 chars
        x0 = alpha_num[move[0]]
        y0 = int(move[1]) - 1

        x1 = alpha_num[move[2]]
        y1 = int(move[3]) - 1

        res = False  # result of evaluation
        the_move = (int, int, int, int)  # (y1 , x1 , midlePice_y , midlePice_x) midle will be removed!

        if (x0 < 0 or x0 > 7 or y0 < 0 or y0 > 7) or (x1 < 0 or x1 > 7 or y1 < 0 or y1 > 7):
            print("our of range cell")
            return False  # out of Bound move .....

        cell_start = board.cells[y0][x0]
        cell_end = board.cells[y1][x1]

        if (
                not cell_start.isActive) or cell_start.color == Constants.UNKNOWN or cell_start.get_player_color() != color:  # start cell should has color
            print("Cannot choose this cell (start)")
            return False  # cell is not Editable or difrent color than player!

        if (
                not cell_end.isActive) or cell_end.get_player_color() == Constants.RED or cell_end.get_player_color == Constants.BLACK:
            print("Cannot choose this cell (end)")
            return False  # cell is not Editable or difrent color than player!

        list_nearbors = self.list_near_moves(board, y0, x0, color, cell_start.isKing())
        # print("neabors :", list_nearbors)

        for (yy, xx, yyy, xxx) in list_nearbors:
            if cell_end.y == yy and cell_end.x == xx:
                if checkOnly:  # do this move so ....
                    # print("THIS CAN BE DON ", color)
                    return True
                else:  # only check this move ...
                    if yyy != -1 and xxx != -1:  # should cross over -> Turn Won't Change
                        board.cells[yyy][xxx].color = Constants.UNKNOWN  # kill middle pice
                        # board.cells[yyy][xxx].condition = 0  # empty cell
                        # self.__turn = color  # user did it (turn wont change)
                    else:  # should change turn ----------
                        self.change_turn()
                    cell_end.color = cell_start.color
                    cell_start.color = Constants.UNKNOWN
                    # cell_start.condition = 0 # clean this cell
                    # cell_end.color = color
                    if (cell_start.isKing()) or (cell_end.get_player_color() == Constants.RED and cell_end.y == 7) or (
                            cell_end.get_player_color() == Constants.BLACK and cell_end.y == 0):
                        cell_end.set_king()
                    # print("MOOVE DON ", color)
                    return True

        # all cases this move is forbidden
        print("NOTHING WILL HAPPEN")
        return False

    # check if game is Finished & return the Winner Color
    def isGameFinished(self, board: Board) -> str:
        count_red = 0
        count_black = 0
        for i in range(8):
            for j in range(8):
                cell = board.cells[i][j]
                if cell.isActive:
                    if cell.get_player_color() == Constants.RED:
                        count_red += 1
                    elif cell.get_player_color() == Constants.BLACK:
                        count_black += 1

        if count_red == 0:
            return Constants.BLACK
        if count_black == 0:
            return Constants.RED
        if self.is_draw():
            return Constants.DRAW
        if self.__looser_ai_color == Constants.RED:  # rend has no move
            return Constants.BLACK
        if self.__looser_ai_color == Constants.BLACK:  # black has no move
            return Constants.RED

        return Constants.UNKNOWN  # this mean winner is unknown
