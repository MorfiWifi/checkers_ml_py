"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2019
morfi dose'nt like copy ! [ so it's different !!]
out put values are normally bit mod (true , false)
Attribute Information:
1. sepal length in cm
2. sepal width in cm
3. petal length in cm
4. petal width in cm
5. class:
-- Iris Setosa
-- Iris Versicolour
-- Iris Virginica

----> * --|      |
----> * --| HIDEN|--> C1
----> * --|      |--> C2
----> * --|      |--> C3
 NET is Full Connected!
"""
import math, numpy as np, random, csv

# static Values ---------------------------------
UNKNOWN = "unknown"
VALIDITY = "validity"
SETOSA = "Iris-setosa"
VERSICOLOR = "Iris-versicolor"
VIRGINICA = "Iris-virginica"
LEARN_SET_CSV_PATH = "iris.csv"  # this is full data base!!
DATA_SET_SIZE = 150  # size of data
TRAIN_SET_CSV_PATH = "iris.csv"
LEARN_RATE = 0.1
HIDDENLAYER_NUM = 1
HIDDENLAYER_LENGTH = 3
OURPUTLAYER_LENGTH = 3
INPUTLAYER_LENGTH = 4
ITARATION_NET = 1000
global INDEX
INDEX = 0  # Jus a simple Identifier
# static values -----------------------------------------

# global Property for App -------------------------
AttributeTypes = []
fullData = []  # all data in CSV File
trainData = []  # simple array for CSV File
trainLables = []
fullDataLables = []
classes = {SETOSA: 1, VERSICOLOR: 2, VIRGINICA: 3}
revese_class = {1: SETOSA, 2: VERSICOLOR, 3: VIRGINICA}
revers_map = {0: "sepal_length", 1: "sepal_width", 2: "petal_length", 3: "petal_width", 4: "class"}
forward_map = {"sepal_length": 0, "sepal_width": 1, "petal_length": 2, "petal_width": 3, "class": 4}


# read Randomly From DataSet as TrainSet
# cant Guarantee 100% read or Exactly all Percentage!
def initial_data_set(percent_of_read: float):
    amount = percent_of_read / 100
    size = int(DATA_SET_SIZE * amount)
    print("train Random count:" , size)
    should_read = []
    for ii in range(size):
        random_index = random.randint(0, DATA_SET_SIZE - 1)
        should_read.append(random_index)
    should_read.sort()
    should_read = list(dict.fromkeys(should_read))  # remove redundancy

    trainData.clear()
    with open(LEARN_SET_CSV_PATH) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            for index in should_read:
                if index < line_count:
                    continue  # already passed this [cant happen ]
                if index == line_count:
                    # add this line to train set
                    trainData.append([float(row[0]), float(row[1]), float(row[2]), float(row[3])])
                    trainLables.append(lable_to_net_out(classes[row[4]]))
                    # print("train name:", row[4])
                    # print("train lable:", lable_to_net_out(classes[row[4]]))
                    line_count += 1
                    break

                if index > line_count:
                    line_count += 1
                    break  # Go next line in CSV
    # load train file
    fullData.clear()
    with open(LEARN_SET_CSV_PATH) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for rowi in csv_reader:
            if line_count >= DATA_SET_SIZE:
                break
            fullData.append([float(rowi[0]), float(rowi[1]), float(rowi[2]), float(rowi[3])])
            fullDataLables.append(lable_to_net_out(classes[rowi[4]]))
            # print("net_out :", lable_to_net_out(classes[rowi[4]]))
            # print("class name :", rowi[4])
            line_count += 1

    print("TrainSet Size:", len(trainData))
    print("DataSet Size:", len(fullData))


# convert net out array to class index
def net_out_to_class(out_put: []):
    if out_put == [1, 0, 0]:
        return 1
    if out_put == [0, 1, 0]:
        return 2
    if out_put == [0, 0, 1]:
        return 3
    return -1


# convert class index to net out array
def lable_to_net_out(class_index: int):
    if class_index == 1:
        return [1, 0, 0]
    if class_index == 2:
        return [0, 1, 0]
    if class_index == 3:
        return [0, 0, 1]
    return [0, 0, 0]


class Connection:
    def __init__(self, connectedNeuron):
        self.connectedNeuron = connectedNeuron
        self.weight = np.random.normal()
        self.dWeight = 0.0


class Cell:
    alpha = 0.01

    def __init__(self, previous_layer):
        self.connections = []
        self.error = 0.0
        self.gradient = 0.0
        self.output = 0.0
        if previous_layer is None:
            pass
        else:
            for neuron in previous_layer:
                con = Connection(neuron)
                self.connections.append(con)

    def more_error(self, extra_error):
        self.error = self.error + extra_error

    def sigmoid(self, x):
        return 1 / (1 + math.exp(-x * 1.0))

    def sigmoid_prim(self, x):
        return x * (1.0 - x)

    def save_error(self, error):
        self.error = error

    def save_output(self, output):
        self.output = output

    def calculate_output(self):
        final_output = 0
        if len(self.connections) == 0:
            return
        for connection in self.connections:
            final_output = final_output + connection.connectedNeuron.output * connection.weight
        self.output = self.sigmoid(final_output)

    def update(self):
        self.gradient = self.error * self.sigmoid_prim(self.output)
        for connection in self.connections:
            connection.dWeight = Cell.alpha * (
                    connection.connectedNeuron.output * self.gradient) + self.alpha * connection.dWeight
            connection.weight = connection.weight + connection.dWeight
            connection.connectedNeuron.more_error(connection.weight * self.gradient)
        self.error = 0


class Net:
    def __init__(self, nod_in_layers):
        self.layers = []
        for num_neuron in nod_in_layers:
            layer = []
            for i in range(num_neuron):
                if len(self.layers) == 0:
                    layer.append(Cell(None))
                else:
                    layer.append(Cell(self.layers[-1]))
            layer.append(Cell(None))  # bias neuron
            layer[-1].save_output(1)  # setting output of bias neuron as 1
            self.layers.append(layer)

    def set_input(self, inputs):
        for i in range(len(inputs)):
            self.layers[0][i].save_output(inputs[i])

    def calculate_error(self, target):
        err = 0
        for i in range(len(target)):
            e = (target[i] - self.layers[-1][i].output)
            err = err + e ** 2
        err = err / len(target)
        err = math.sqrt(err)
        return err

    def feet_forward(self):
        for layer in self.layers[1:]:
            for neuron in layer:
                neuron.calculate_output()

    def back_propagation(self, target):
        for i in range(len(target)):
            self.layers[-1][i].save_error(target[i] - self.layers[-1][i].output)
        for layer in self.layers[::-1]:  # reverse the order
            for neuron in layer:
                neuron.update()

    def calculate_output(self):
        output = []
        for neuron in self.layers[-1]:
            o = neuron.output
            if o > 0.5:
                o = 1
            else:
                o = 0
            output.append(o)
        output.pop()
        return output


def print_train_data():
    for data, lable in zip(trainData, trainLables):
        print("tr data:", data, " lable:", lable)


def print_real_data():
    for data, lable in zip(fullData, fullDataLables):
        print("tr data:", data, " lable:", lable)


# main ---------------------------------------------
initial_data_set(150.0)  # number of random choose from data set / in pr
net_shape = [4, 4, 3]
net = Net(net_shape)
inputs = trainData
outputs = trainLables
iterations = 10000
min_error = 0.01
print_train_data()
# print_real_data()
for j in range(iterations):
    error = 0
    for i in range(len(inputs)):
        net.set_input(inputs[i])
        net.feet_forward()
        net.back_propagation(outputs[i])
        error = error + net.calculate_error(outputs[i])
    print("iteration: ", j, " error: ", error)
    if error < min_error:
        break

# test --------
correct = 0
wrong = 0
total = 0
for data, lable in zip(fullData, fullDataLables):
    net.set_input(data)
    net.feet_forward()
    out = net.calculate_output()
    print({"INPUT:": data, "Lable:": lable, "NET:": out})
    if out == lable:
        correct += 1
    else:
        wrong += 1
    total += 1
print({"Total :": total, "Correct :": correct, "Wrong :": wrong})
