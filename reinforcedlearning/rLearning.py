"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2020
morfi dose'nt like copy ! [ so it's different !!]

I have nothing to say at time!
Q-learning (oowi how can i Do it ?) [can it converg ? ] ... it,s a mistiry for me ...
Agent can get Out of Board! get -100 point ! is it right ? [Assume yes!]
"""
import random, copy, numpy as np

learning_rate = 0.4
discount_factor = 0.8

epoc_count = 10000
epoc_max_steps = 120

# o = out of borders خارج از مرزها
# A = any move!
score_map = {'G': 100, 'W': -100, 'H': -100, 'O': -100, 'A': -1, 'E': -1}
action_map = {1: 'R', 2: 'L', 3: 'U', 4: 'D'}
# moves = 1 R , 2 L , 3 Up , 4 Down

print_map = {'G': 'G', 'E': ' ', 'H': 'H', 'W': 'W', 'A': '$'}
empty_cell_char = 'E'
agent_cell_char = 'A'

ground = [['G', 'E', 'H', 'E', 'E'],
          ['E', 'E', 'E', 'E', 'H'],
          ['W', 'E', 'E', 'E', 'E'],
          ['E', 'E', 'E', 'H', 'E'],
          ['E', 'E', 'E', 'E', 'E']]

current_index = [4, 0]  # y & x

for i in range(100):
    rand_num = random.randint(0, 2)
    # print(rand_num)


class Agent:
    def __init__(self):
        self.iteration = 1
        self.explor_rate = 1 / (self.iteration * 0.1)  # reduce explorr rate bytime!
        self.pre_set_value = -1
        self.Q_table = np.ones((25, 4), dtype=float)
        self.state_value = np.ones(25, dtype=float)
        self.state = [4, 0]
        self.episode_moves = 0
        self.finish_epoc = False

    def initialize_pre_values(self):
        self.Q_table = np.array(self.Q_table) * self.pre_set_value
        self.state_value = np.array(self.state_value) * self.pre_set_value

    def nex_move(self) -> int:
        rnd = random.random()
        print("rnd:", rnd, "ExplorRate:", self.explor_rate)
        if self.explor_rate > rnd:  # choose next move random
            best_move = random.randint(1, 4)
            return best_move
        else:  # choose next moove base on table
            best_move = 1
            last_move_value = float('-inf')
            for move_num in range(4):
                index = (self.state[0] * 5) + (self.state[1] + 1)
                index -= 1  # reduce one to start from 0 till len -1
                # print("Q_TIndex:", index)
                cell_move_value = self.Q_table[index][move_num]
                if cell_move_value > last_move_value:
                    best_move = move_num + 1
                    last_move_value = cell_move_value

            return best_move

    def validate_move(self, move: int):
        # save current state value & things ...
        pr_stat = copy.deepcopy(self.state)
        self.updaet_state(move)
        new_state = self.state

        if self.isStateOutOfRange():  # new state is out of Range
            self.update_q_table(-1, self.state_to_index(pr_stat), move, -100)
            self.finish_epoc = True
        else:
            reward = score_map[ground[self.state[0]][self.state[1]]]

            if reward != -1:  # it was non Empty State!
                self.finish_epoc = True

            new_state_max_rew = float('-inf')
            for i in range(4):
                if self.Q_table[self.state[0]][i] > new_state_max_rew:
                    new_state_max_rew = self.Q_table[self.state[0]][i]

            # print("newState:", self.state, "rew:", reward, "S'a:", new_state_max_rew)
            self.update_q_table(reward, self.state_to_index(pr_stat), move, new_state_max_rew)

        self.episode_moves += 1

    def update_q_table(self, reward, stae_num: int, move_num: int, new_state_val):
        value_of_state = self.Q_table[stae_num][move_num - 1] * (1 - learning_rate) \
                         + learning_rate * (reward + discount_factor * new_state_val)
        update_index = [stae_num, move_num - 1]

        self.Q_table[stae_num][move_num - 1] = value_of_state
        # print("Q_T:", stae_num, "AC:", move_num - 1, "val:", self.Q_table[stae_num][move_num - 1])

    def updaet_state(self, move: int):
        if move == 1:
            self.state[1] += 1
        if move == 2:
            self.state[1] -= 1
        if move == 3:  # up in Array is Reduce index !!
            self.state[0] -= 1
        if move == 4:  # Down in Array is Increase Index !!
            self.state[0] += 1

    def isStateOutOfRange(self) -> bool:
        # print("OTFR_0:", self.state[0], "1:", self.state[1])
        if 5 > self.state[0] >= 0 and 5 > self.state[1] >= 0:
            return False
        # if score_map[ground[self.state[0]][self.state[1]]] != -1:
        #     return score_map[ground[self.state[0]][self.state[1]]]
        return True

    def isEpocFinished(self):
        if self.episode_moves > epoc_max_steps:
            return True
        return self.finish_epoc

    # state starts from 0 till 24 (len = 25)
    def state_to_index(self, state) -> int:
        res_index = (state[0] * 5) + (state[1] + 1)
        # print("state:", state, "index:", res_index - 1)
        return res_index - 1
        # return (state[1] * 4) + (state[0])

    def start_new_epoc(self):
        self.state = [4, 0]  # begin from start !
        self.episode_moves = 0
        self.finish_epoc = False
        self.iteration += 1
        self.explor_rate = 1 / (self.iteration * 0.1)  # reduce Explorer rate

    def print_q_table(self):
        ys = []
        xs = []
        for i in range(5):
            ys.append(i + 1)
            for j in range(5):
                xs.append(j + 1)

        print("STATE |   R    |   L    |   U    |   D   ")
        print('______|________|________|________|________')

        for i in range(25):
            # for j in range(5):
            # print(str(i * 4 + j))
            print('  {:1d},{:1d} | {:06.1f} | {:06.1f} | {:06.1f} | {:06.1f} '.format(
                ys[int(i / 5)], xs[int(i % 5)], self.Q_table[i][0], self.Q_table[i][1],
                self.Q_table[i][2], self.Q_table[i][3]))
            print('      |        |        |        |       ')
            print('______|________|________|________|________')


# prints Ground of play in proper format
def print_board(agent: Agent):
    groundi = copy.deepcopy(ground)
    if agent.isStateOutOfRange():
        print("AGENT OUT OF BOUNDS")
        return
    groundi[agent.state[0]][agent.state[1]] = 'A'  # this is Me !

    for i in range(5):
        if i == 0:
            print(' _____ _____ _____ _____ _____ ')

        print('|     |     |     |     |     |')
        print(
            '|  {:s}  |  {:s}  |  {:s}  |  {:s}  |  {:s}  |'.format(print_map[groundi[i][0]], print_map[groundi[i][1]],
                                                                    print_map[groundi[i][2]], print_map[groundi[i][3]],
                                                                    print_map[groundi[i][4]]))
        print('|_____|_____|_____|_____|_____|')


# mian --------------------------------------------
# print_board()

value_table_grpund = 0

agent = Agent()
agent.pre_set_value = -1
agent.initialize_pre_values()
for i in range(epoc_count):
    while ~agent.isEpocFinished():
        print("Agentstate:", agent.state)
        move = agent.nex_move()
        print("move:", move)
        agent.validate_move(move)

        print_board(agent)

        if agent.isEpocFinished():
            agent.start_new_epoc()
            print_board(agent)
            break

agent.print_q_table()

# agent.print_q_table()

# print_board(agent)
# move = agent.nex_move()
# print("move:", move)
# agent.validate_move(move)
# # print_board(agent)
#
# print("is Finished:", agent.isEpocFinished())
#
# agent.print_q_table()
#
# if agent.isEpocFinished():
#     agent.start_new_epoc()
