"""
Created by MorfiWifi aka moteza eydipour (9811634)
KNT Univercity
2019
morfi dose'nt like copy ! [ so it's different !!]

causion : renamed Data Set to DataSet (for more easy addressing!)
"""
# constants ---------------------
prefix = "DataSet"
class_name = ['1', '2', '3', '4', '5', '6']
TEST_FOLDER_NAME = "Test"
TRAIN_FOLDER_NAME = "Train"
CLASS_START_INDEXES = [38214, 74720, 101551, 15168, 20487, 54116]
TEST_SET_LENGTH = 15  # exactly 15
TRAIN_SET_LENGTH = 100  # exactly 100
CLASS_COUNT = 6
FULL_DATA_SET = []  # all dictionaryes will be here!
FULL_ATTRIBURES = []  # 120 Word Array
train_set = []
test_set = []

# train set is after test set


# functions ------------------
import re, copy


def build_attributes():
    for i in range(0, 6):
        class_dict = FULL_DATA_SET[i]
        index = 0
        for word in class_dict:
            if index >= 20:
                break
            FULL_ATTRIBURES.append(word)
            index += 1
    print("attributes:", FULL_ATTRIBURES)


def build_arff_train():
    print("building attf train file")
    f = open("train_set.arff", "w")
    f.write("@relation textprocess\n")
    for i in range(0, 60):
        f.write("@attribute a_" + str(i) + " numeric\n")
    f.write("@attribute class {1,2,3,4,5,6,7}\n@data\n")

    for i in range(0, CLASS_COUNT):
        print("working on class:", i)
        for j in range(0, TRAIN_SET_LENGTH):
            path = "./" + prefix + "/" + class_name[i] + "/" + TRAIN_FOLDER_NAME + "/" + str(
                CLASS_START_INDEXES[i] + j + TEST_SET_LENGTH)
            print("paht:", path)
            # openfile
            train_file = open(path, "r")
            full_text = train_file.read()  # reading doc as single STR
            full_text = full_text.lower()  # all to lower case

            for k in range(0, 60):
                clumn = FULL_ATTRIBURES[k]
                count = full_text.count(clumn)  # repeat of this word!
                # print(clumn, count, " times in ", path)
                f.write(str(count) + ",")
            f.write(str(i + 1))  # class lable
            f.write("\n")
    f.close()
    print("train Arff ha built")


def build_arff_test():
    print("building attf test file")
    f = open("test_set.arff", "w")
    f.write("@relation textprocess_test\n")
    for i in range(0, 60):
        f.write("@attribute a_" + str(i) + " numeric\n")
    f.write("@attribute class {1,2,3,4,5,6,7}\n@data\n")

    for i in range(0, CLASS_COUNT):
        print("working on class:", i)
        for j in range(0, TEST_SET_LENGTH):
            path = "./" + prefix + "/" + class_name[i] + "/" + TEST_FOLDER_NAME + "/" + str(
                CLASS_START_INDEXES[i] + j)
            print("paht:", path)
            # openfile
            train_file = open(path, "r")
            full_text = train_file.read()  # reading doc as single STR
            full_text = full_text.lower()  # all to lower case

            for k in range(0, 60):
                clumn = FULL_ATTRIBURES[k]
                count = full_text.count(clumn)  # repeat of this word!
                # print(clumn, count, " times in ", path)
                f.write(str(count) + ",")
            f.write(str(i + 1))  # class lable
            f.write("\n")
    f.close()
    print("test Arff ha built")


def callculate_effecent_count(FULL_DATA_SET):
    temp_full_data = copy.deepcopy(FULL_DATA_SET)
    for i in range(0, 6):
        for word in FULL_DATA_SET[i]:
            # print("word:", word, " i:", i)
            summm = 0
            for j in range(0, 6):
                if i == j:
                    continue
                summm += FULL_DATA_SET[j].get(word, 0)

            temp_full_data[i].setdefault(word, 0)
            temp_full_data[i][word] -= summm

    print("FULL DATA:", temp_full_data)
    return temp_full_data


def sort_data_set_dicts(FULL_DATA_SET):
    new_data_set = []
    for dicti in FULL_DATA_SET:
        sorted_x = sorted(dicti.items(), key=lambda kv: kv[1], reverse=True)
        new_data_set.append(dict(sorted_x))

    return new_data_set


def write_efectives_on_file(ef_count_lsit):
    index = 1
    for dicti in ef_count_lsit:
        f = open("result_for_" + str(index) + ".txt", "w")
        inner_index = 0
        for word in dicti.keys():
            if inner_index >= 20:
                break
            f.write(word + " : " + str(dicti[word]) + "\n")
            inner_index += 1
        f.close()
        index += 1


def star_the_job():
    print("starting.....")
    for i in range(0, CLASS_COUNT):
        print("working on class:", i)
        class_dic = {}
        for j in range(0, TRAIN_SET_LENGTH):
            path = "./" + prefix + "/" + class_name[i] + "/" + TRAIN_FOLDER_NAME + "/" + str(
                CLASS_START_INDEXES[i] + j + TEST_SET_LENGTH)
            # print("paht:", path)
            # openfile
            train_file = open(path, "r")
            full_text = train_file.read()  # reading doc as single STR
            full_text = full_text.lower()  # all to lower case
            # preProcess removig invalid data
            full_text = re.sub('\S+@\S+', "", full_text)  # remove emails
            full_text = re.sub('\d*', "", full_text)  # remove numbers
            full_text = re.sub('\*|<|>|;|,|\"|\'|@|!|#|%|\^|&|\(|\)|=|-|_|\?|\/|\\\|~|\+|\`|:', "",
                               full_text)  # remove symbolss
            # print("doc before Long REMOVEr:", full_text)
            full_text = re.sub('[a-zA-Z.]{15,}', "", full_text)  # remove long text
            full_text = re.sub('\.', " ", full_text)  # remove . to " " (space)

            # print("doc after cleaning:", full_text)

            bag_words = re.findall('[a-zA-Z]{3,}', full_text)  # texts longet than 3 W

            for word in bag_words:
                class_dic.setdefault(word, 0)  # if dosent have word set 0
                class_dic[word] += full_text.count(word)
        sorted_x = sorted(class_dic.items(), key=lambda kv: kv[1], reverse=True)
        # sorted From max
        # print("class:", i + 1, " wordCount:", class_dic)
        FULL_DATA_SET.append(dict(sorted_x))


# main -----------------------

star_the_job()
FULL_DATA_SET = callculate_effecent_count(FULL_DATA_SET)
FULL_DATA_SET = sort_data_set_dicts(FULL_DATA_SET)
write_efectives_on_file(FULL_DATA_SET)
build_attributes()

build_arff_train()
build_arff_test()
