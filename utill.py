"""
Created by MorfiWifi AKA morteza eydipour (9811634)
2019 - KNT Uni
"""
WINDWOS_CMD = False  # in Windwos CMD Colors Wont Work!-> [Black BB BK White RR RK]
alpha_num = {"a": 0, "b": 1, "c": 2, "d": 3, "e": 4, "f": 5, "g": 6, "h": 7}
alpha_num_rev = {0: "a", 1: "b", 2: "c", 3: "d", 4: "e", 5: "f", 6: "g", 7: "h"}


class Constants:
    WHITE = "white"
    BLACK = "black"
    RED = "red"
    DRAW = "draw"
    UNKNOWN = "unknown"
    normal_me = "normal_me     "
    king_me = "king_me       "
    normal_enemy = "normal_enemy  "
    king_enemy = "king_enemy    "
    will_die_me = "will_die_me   "
    will_die_enemy = "will_die_enemy"
    base = "base"
    BOARD_VALUE = "board_value"

    if WINDWOS_CMD:
        BLACK_NORMAL = "BB"
        BLACK_KING = "BK"
        RED_NORMAL = "RR"
        RED_KING = "RK"
    else:
        BLACK_NORMAL = "XB"
        BLACK_KING = "KB"
        RED_NORMAL = "XR"
        RED_KING = "KR"

# write all game stats on file
def save_on_file_log(message: str):
    f = open("checkers_logs.txt", "a")
    f.write(message + "\n")
    f.flush()
    f.close()